export default {
  pages: {
    home: {
      title: 'Home',
      headline: 'Welcome to the Headless Strapi.io, VueJS & Nuxt Demo',
    },
    signup: {
      title: 'Signup',
      headline: 'Signup for a new account',
    },
    admin: {
      login: {
        title: 'Admin Login',
        headline: 'Admin Login',
        loggedIn: 'Logged in',
        redirectUser: 'Redirecting you to the homepage in',
        seconds: 'seconds'
      }
    },
    login: {
      title: 'User Login',
      headline: 'User Login',
      loggedIn: 'Logged in',
      redirectUser: 'Redirecting you to the homepage in',
      seconds: 'seconds'
    },
    logout: {
      title: 'Logout',
      headline: 'Logout',
    },
    profile: {
      title: 'Profile',
      headline: 'My Profile',
    },
    articles: {
      title: 'Articles',
      headline: 'Articles overview'
    }
  },
  components: {
    articlesList: {
      headline: 'Articles',
      notLoggedIn: 'Please signup and/ or login to view the articles',
      updating: 'Updating',
      successfullyUpdated: 'Succesfully updated',
      errorUpdating: 'Error updating'
    }
  },
  buttonLabels: {
    home: 'Home',
    signup: 'Signup',
    adminLogin: 'Admin Login',
    userLogin: 'User Login',
    logout: 'Logout',
    readMore: 'Read more',
    archive: 'Archive',
    publish: 'Publish',
    draft: 'Draft',
    close: 'Close',
    articles: 'Articles',
    allArticles: 'All articles'
  },
  formLabels: {
    login: {
      username: 'Username',
      email: 'E-mail',
      usernameOrEmail: 'Username or E-mail',
      password: 'Password'
    },
    signup: {
      username: 'Username',
      email: 'E-mail',
      password: 'Password'
    },
  }
}
