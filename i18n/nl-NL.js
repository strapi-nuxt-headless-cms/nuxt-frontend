export default {
  pages: {
    home: {
      title: 'Home',
      headline: 'Welkom bij de Headless Strapi.io, VueJS & Nuxt Demo',
    },
    signup: {
      title: 'Registreren',
      headline: 'Registreren',
    },
    admin: {
      login: {
        title: 'Admin Login',
        headline: 'Admin Login',
        loggedIn: 'Ingelogd',
        redirectUser: 'We sturen je automatisch naar de homepage in',
        seconds: 'seconden'
      }
    },
    login: {
      title: 'Inloggen',
      headline: 'Inloggen',
      loggedIn: 'Ingelogd',
      redirectUser: 'We sturen je automatisch naar de homepage in',
      seconds: 'seconden'
    },
    logout: {
      title: 'Uitloggen',
      headline: 'Uitloggen',
    },
    profile: {
      title: 'Profiel',
      headline: 'Profiel',
    },
    articles: {
      title: 'Artikelen',
      headline: 'Artikelen overzicht'
    }
  },
  components: {
    articlesList: {
      headline: 'Artikelen',
      notLoggedIn: 'Registeer en/of login om de artikelen te bekijken',
      updating: 'Updating',
      successfullyUpdated: 'Successvol geupdated',
      errorUpdating: 'Fout bij updaten van'
    }
  },
  buttonLabels: {
    home: 'Thuis',
    signup: 'Registeren',
    adminLogin: 'Admin Login',
    userLogin: 'User Login',
    logout: 'Uitloggen',
    readMore: 'Meer lezen',
    archive: 'Archiveren',
    publish: 'Publiseren',
    draft: 'Concept',
    close: 'Sluit',
    articles: 'Artikelen',
    allArticles: 'Alle artikelen'
  },
  formLabels: {
    login: {
      username: 'Gebruikersnaam',
      email: 'E-mail',
      usernameOrEmail: 'Gebruikersnaam of E-mail',
      password: 'Wachtwoord'
    },
    signup: {
      username: 'Gebruikersnaam',
      email: 'E-mail',
      password: 'Wachtwoord'
    },
  }
}
