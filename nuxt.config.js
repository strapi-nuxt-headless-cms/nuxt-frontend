import axios from 'axios'
import path from 'path'
import fs from 'fs'
import colors from 'vuetify/es5/util/colors'


const ENV_FILE =
  process.env.NODE_ENV === 'production'
    ? '.env.prd'
    : process.env.NODE_ENV === 'staging'
      ? '.env.stg'
      : '.env'

require('dotenv').config({ path: `${process.env.PWD}/${ENV_FILE}` })

export default {
  /*
   ** Nuxt rendering mode
   ** See https://nuxtjs.org/api/configuration-mode
   */
  mode: 'universal',
  /*
   ** Nuxt target
   ** See https://nuxtjs.org/api/configuration-target
   */
  target: 'server',
  /*
   ** Headers of the page
   ** See https://nuxtjs.org/api/configuration-head
   */
  head: {
    meta: [
      { charset: 'utf-8' },
      { name: 'viewport', content: 'width=device-width, initial-scale=1' },
      {
        hid: 'description',
        name: 'description',
        content: 'Default description',
      },
    ],
    link: [{ rel: 'icon', type: 'image/x-icon', href: '/favicon.ico' }],
  },
  /*
   ** Env
   */
  publicRuntimeConfig: {
    baseUrl: process.env.BASE_URL || 'https://localhost:3000',
    cmsUrl: process.env.CMS_URL || 'http://localhost:1337',
    cmsGraphQLuri: process.env.CMS_GRAPHQL_URI || '/graphql',
    siteName: 'Headless CMS',
    siteOwner: 'Frontmen - Intracto'
  },
  privateRuntimeConfig: {

  },
  /*
   ** Global CSS
   */
  css: [],
  /*
   ** Telemetry
   */
  telemetry: false,
  /*
   ** Plugins to load before mounting the App
   ** https://nuxtjs.org/guide/plugins
   */
  plugins: [
    '~/plugins/vue-jsonLd.js',
    '~/plugins/vue-mixins.js',
  ],
  /*
   ** Auto import components
   ** See https://nuxtjs.org/api/configuration-components
   */
  components: true,
  /*
   ** Nuxt.js dev-modules
   */
  buildModules: [
    ['@nuxtjs/dotenv', { filename: ENV_FILE }],
    '@nuxtjs/vuetify',
    '@nuxtjs/moment'
  ],
  /*
   ** Nuxt.js modules
   */
  modules: [
    '@nuxtjs/axios',
    '@nuxtjs/pwa',
    'nuxt-i18n',
    '@nuxtjs/apollo',
    '@nuxtjs/strapi',
    '@nuxtjs/markdownit',
    '@nuxtjs/robots',
    '@nuxtjs/sitemap',
    '@nuxtjs/auth'
  ],
  server: {
    https: {
      key: fs.readFileSync(path.join(__dirname, 'certs', 'server', 'localhost.key')),
      cert: fs.readFileSync(path.join(__dirname, 'certs', 'server', 'localhost.crt'))
    }
  },
  /*
   ** Markdown
   **
  */
  markdownit: {
    preset: 'default',
    linkify: true,
    breaks: true,
    injected: true
  },
  /*
   ** Axios module configuration
   ** See https://axios.nuxtjs.org/options
   */
  axios: {
    baseURL: process.env.CMS_URL || 'http://localhost:1337',
    browserBaseURL: process.env.CMS_URL || 'http://localhost:1337',
    https: false
  },
  /*
   ** Robots.txt
   ** See https://www.npmjs.com/package/@nuxtjs/robots
   */
  robots: {
    UserAgent: `${process.env.ROBOTS_USERAGENT}`,
    CrawlDelay: `${process.env.ROBOTS_CRAWLDELAY}`,
    Allow: `${process.env.ROBOTS_ALLOW}`,
    Disallow: `${process.env.ROBOTS_DISALLOW}`,
    Host: `${process.env.BASE_URL}`,
    Sitemap: `${process.env.BASE_URL}${process.env.SITEMAP_URI}`,
    CleanParam: `${process.env.ROBOTS_CLEANPARAM}`
  },
  /*
   ** Sitemap
   ** See https://www.npmjs.com/package/strapi-plugin-sitemap
   */
  sitemap: {
    gzip: true,
    generate: false,
    cacheTime: 15 * 1000 * 60,
    exclude: [
      '/**/admin/**'
    ],
    routes: async () => {
      const getJSON = uri => axios.get(`${process.env.CMS_URL}${uri}`).then(r => r.data).catch(e => {
        // return an empty array and log the error
        console.error(`Error while fetching "${uri}" for sitemap`, e);
        return [];
      });

      // Example
      // @TODO: Get public content types from CMS and grab the slugs
      const articles = await getJSON('/articles');
      // @TODO: Fix so the languagess are not hardcoded, best to do when Strapi supports multi-lang?
      return [
        ...articles.map(e => `/en/articles/${e.slug}`),
        ...articles.map(e => `/nl/articles/${e.slug}`),
      ]
    }
  },
  /*
   ** Strapi Module
   ** See https://strapi.nuxtjs.org/options
   */
  strapi: {
    url: process.env.CMS_URL || 'http://localhost:1337'
  },
  apollo: {
    watchLoading: '~/plugins/apollo-watch-loading-handler.js',
    errorHandler: '~/plugins/apollo-error-handler.js',
    // required
    clientConfigs: {
      default: '~/apollo/config.js',
    },
    cookieAttributes: {
      /**
       * Define when the cookie will be removed. Value can be a Number
       * which will be interpreted as days from time of creation or a
       * Date instance. If omitted, the cookie becomes a session cookie.
       */
      expires: 7, // optional, default: 7 (days)

      /**
       * Define the path where the cookie is available. Defaults to '/'
       */
      path: '/', // optional
      /**
       * Define the domain where the cookie is available. Defaults to
       * the domain of the page where the cookie was created.
       */
      // domain: 'example.com', // optional

      /**
       * A Boolean indicating if the cookie transmission requires a
       * secure protocol (https). Defaults to false.
       */
      secure: false,
    },
  },
  /*
   ** vuetify module configuration
   ** https://github.com/nuxt-community/vuetify-module
   */
  vuetify: {
    customVariables: ['~/assets/variables.scss'],
    theme: {
      dark: false,
      themes: {
        dark: {
          primary: colors.blue.darken2,
          accent: colors.grey.darken3,
          secondary: colors.amber.darken3,
          info: colors.teal.lighten1,
          warning: colors.amber.base,
          error: colors.deepOrange.accent4,
          success: colors.green.accent3,
        },
      },
    },
  },
  /*
   ** i18n module configuration
   */
  i18n: {
    locales: [
      {
        name: 'English',
        code: 'en',
        iso: 'en-US',
        file: 'en-US.js',
        isCatchallLocale: true,
      },
      {
        name: 'Nederlands',
        code: 'nl',
        iso: 'nl-NL',
        file: 'nl-NL.js',
      },
    ],
    lazy: true,
    baseUrl: process.env.BASE_URL,
    langDir: 'i18n/',
    defaultLocale: 'en',
    strategy: 'prefix',
    vuex: {
      // Module namespace
      moduleName: 'i18n',
      // If enabled, current app's locale is synced with nuxt-i18n store module
      syncLocale: false,
      // If enabled, current translation messages are synced with nuxt-i18n store module
      syncMessages: false,
      // Mutation to commit to set route parameters translations
      syncRouteParams: true
    },
  },
  moment: {
    defaultLocale: 'en',
    locales: ['nl'],
    timezone: true
  },
  /*
   ** Nuxt Auth Module
   ** See https://auth.nuxtjs.org/
   */
  auth: {
    localStorage: false,
    redirect: {
      login: false,
      logout: false,
      callback: false,
      home: false
    },
    strategies: {
      localUser: {
        _scheme: 'local',
        endpoints: {
          login: { url: '/auth/local', method: 'post', propertyName: 'jwt' },
          // logout: { url: '/api/auth/logout', method: 'post' },
          user: { url: '/users/me', method: 'get' }
        },
        tokenRequired: true,
        globalToken: true,
        tokenType: 'Bearer',
        autoFetchUser: true
      },
      adminUser: {
        _scheme: 'local',
        endpoints: {
          login: { url: '/admin/login', method: 'post', propertyName: 'data.token' },
          // logout: { url: '/api/auth/logout', method: 'post' },
          user: { url: '/users/me', method: 'get' }
        },
        tokenRequired: true,
        globalToken: true,
        tokenType: 'Bearer',
      }
    }
  },
  /*
   ** Build configuration
   ** See https://nuxtjs.org/api/configuration-build/
   */
  build: {
    hotMiddleware: {
      client: {
        overlay: false,
      },
    },
    babel: {
      presets({ isServer }) {
        return [
          [
            require.resolve('@nuxt/babel-preset-app'),
            {
              buildTarget: isServer ? 'server' : 'client',
              corejs: { version: 3 },
            },
          ],
        ]
      },
    },
  },
}
