import Vue from 'vue';

Vue.mixin({
  methods: {
    getMediaURL(url) {
      if (url.substr(0, 4) !== 'http') {
        return `${process.env.CMS_URL}${url}`;
      }
      return url;
    },
    createSeoTags({ metadata, path }) {
      const meta = [
        {
          hid: 'description',
          name: 'description',
          content: metadata.description || 'Frontmen - Frontend Experts'
        },
        {
          hid: 'keywords',
          name: 'keywords',
          content: metadata.keywords
        },
        {
          hid: 'author',
          name: 'author',
          content: this.$config.siteOwner
        },
        {
          hid: 'og:site_name',
          property: 'og:site_name',
          content: this.$config.siteName
        },
        {
          hid: 'og:title',
          property: 'og:title',
          content: metadata.title
        },
        {
          hid: 'og:description',
          property: 'og:description',
          content: metadata.description
        },
        {
          hid: 'og:type',
          property: 'og:type',
          content: metadata.type
        },
        {
          hid: 'og:url',
          property: 'og:url',
          content: process.env.BASE_URL + (path || '')
        },
        {
          hid: 'og:image',
          property: 'og:image',
          content: this.getMediaURL(metadata.image.formats.medium.url.replace('https', 'http'))
        },
        {
          hid: 'og:image:secure_url',
          property: 'og:image:secure_url',
          content: this.getMediaURL(metadata.image.formats.medium.url)
        },
        {
          hid: 'og:image:alt',
          property: 'og:image:alt',
          content: metadata.title
        },
        {
          hid: 'og:image:alt',
          property: 'og:image:alt',
          content: metadata.image.mime
        },
        {
          hid: 'apple-mobile-web-app-title',
          name: 'apple-mobile-web-app-title',
          content: `${metadata.title} | ${this.$config.siteOwner}`
        },
        ...(metadata.custom || [])
      ];
      return {
        title: `${metadata.title} | ${this.$config.siteOwner}`,
        description: metadata.description,
        meta,
        htmlAttrs: {
          lang: this.$i18n.locale
        }
      }
    }
  }
});
