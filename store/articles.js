import articlesQuery from '~/graphql/articlesQuery.graphql'
import articleUpdateStatusMutation from '~/graphql/articleUpdateStatusMutation.graphql'

export const state = () => ({
  articles: [],
})

export const mutations = {
  storeArticles(state, data) {
    if (data && data.articles) {
      state.articles = data.articles
    }
  },
  clearArticles(state) {
    state.articles = []
  },
  updateArticleStatus(state, { id, status }) {
    state.articles = [...state.articles.map(item => item.id !== id ? item : { ...item, status })]
  }
}

export const actions = {
  getArticles({ commit }) {
    if (this.$auth.loggedIn) {
      console.log(`Fetching articles: User is logged in`)
      const client = this.app.apolloProvider.defaultClient
      client.query({
        query: articlesQuery
      })
        .then(({ data }) => {
        this.articles = data?.articles
        this.articles?.map((article) => (article.fab = false))
        commit('storeArticles', {
          articles: this.articles
        })
      })
    } else {
      console.log(`Not fetching articles: User is not logged in`)
    }
  },
  async updateArticleStatus({ commit }, { id, status }) {
    if (this.$auth.loggedIn) {
      console.log(`Updating article: User is logged in as Admin/ Editor/ Author`)
      const client = this.app.apolloProvider.defaultClient

      const res = await client.mutate({
        mutation: articleUpdateStatusMutation,
        variables: {
          id,
          status
        }
      })
      .then(({ data }) => {
        commit('updateArticleStatus', data.updateArticle.article)
      })
      return true
    } else {
      console.log(`Not updating article: User is not logged in as Admin/ Editor/ Author`)
    }
  }
}

export const getters = {
  articles: (state) => (amount) => {
    if (amount && Number.isInteger(amount)) {
      const spliced = [...state.articles]
      return spliced.splice(0, amount)
    } else {
      return state.articles
    }
  },
  articlesCount: (state) => {
    return state.articles.length
  }
}
