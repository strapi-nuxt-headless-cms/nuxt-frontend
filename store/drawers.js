export const state = () => ({
  leftDrawer: false
})

export const mutations = {
  toggleLeftDrawer(state) {
    state.leftDrawer = !state.leftDrawer
  },
}

export const getters = {
  leftDrawerState: (state) => state.leftDrawer
}
