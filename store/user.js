export const state = () => ({
  isLoggedIn: false,
  isUser: false,
  isAdmin: false,
  isAuthor: false,
  isEditor: false,
  user: {},
})

export const mutations = {
  storeLoggedInUser(state, data) {
    if (data && data.user) {
      state.user = data?.user
      state.isLoggedIn = data?.isLoggedIn
      state.isUser = data?.isUser
      state.isAdmin = data?.isAdmin
      state.isAuthor = data?.isAuthor
      state.isEditor = data?.isEditor
    }
  },
  logoutUser(state) {
    state.user = {}
    state.isLoggedIn = false
    this.$strapi.clearToken()
    this.$apolloHelpers.onLogout()
  },
}

export const actions = {
  async validateLogin({ commit, state }) {
    const token = this.$strapi.getToken()
    if (token) {
      try {
        // Fetch the User
        const user = await this.$strapi.fetchUser()
        // Local User
        if (user.role) {
          commit('storeLoggedInUser', {
            user: user,
            isLoggedIn: true,
            isUser: true,
            isAdmin: false,
            isAuthor: false,
            isEditor: false
          })
          return true
        // Admin user
        } else {
          commit('storeLoggedInUser', {
            user: user,
            isLoggedIn: true,
            isUser: false,
            isAdmin: user?.roles.filter(e => e.code === 'strapi-admin').length > 0 ? true : false,
            isAuthor: user?.roles.filter(e => e.code === 'strapi-author').length > 0 ? true : false,
            isEditor: user?.roles.filter(e => e.code === 'strapi-editor').length > 0 ? true : false
          })
          return true
        }
      } catch (error) {
        console.log(`this.$strapi.fetchUser() error: `, error)
        return false
      }
    }
  },
  async loginLocalUser({ commit, state }, { identifier, password }) {
    try {
      // Try the local login
      const login = await this.$strapi.login({ identifier, password })
      // Check if the login was successfull and we received a JWT Token and User Objects
      if (login?.jwt && login?.user) {
        // Store the JWT Token in a Cookie and Apollo so we can use it in GraphQL/ API Calls
        await this.$apolloHelpers.onLogin(login.jwt)
        // Store the JWT in a session cookie
        await this.$strapi.setToken(login.jwt)
        // Commit the user object to the Store
        commit('storeLoggedInUser', {
          user: login.user,
          isLoggedIn: true,
          isUser: true,
          isAdmin: false,
          isAuthor: false,
          isEditor: false
        })
        // Return true for a successful login
        return true
      } else {
        // Unexpected error, no jwt and/or user
        throw {
          response: {
            error: 'Internal Server Error',
            statusCode: 500,
            data: [{
              id: 'Auth.form.error.unexpected',
              message: 'An unexpected error occured, please contact the System Administrator'
            }],
            message: [{
              id: 'Auth.form.error.unexpected',
              message: 'An unexpected error occured, please contact the System Administrator'
            }]
          }
        }
      }
    } catch (error) {
      throw error.response
    }
  },
  async loginAdmin({ commit, state }, { identifier, password }) {
    try {
      // Try the Admin login
      const req = await this.$strapi.$http.post('http://localhost:1337/admin/login', {
        email: identifier,
        password
      })
      const login = await req.json()
      // Check if the login was successfull and we received a JWT Token and User Objects
      if (login?.data?.token && login?.data?.user) {
        // Store the JWT Token in a Cookie and Apollo so we can use it in GraphQL/ API Calls
        await this.$apolloHelpers.onLogin(login.data.token)
        // Store the JWT in a session cookie
        await this.$strapi.setToken(login.data.token)

        commit('storeLoggedInUser', {
          user: login.data.user,
          isLoggedIn: true,
          isUser: false,
          isAdmin: login.data.user?.roles.filter(e => e.code === 'strapi-admin').length > 0 ? true : false,
          isAuthor: login.data.user?.roles.filter(e => e.code === 'strapi-author').length > 0 ? true : false,
          isEditor: login.data.user?.roles.filter(e => e.code === 'strapi-editor').length > 0 ? true : false
        })
        // Return true for a successful login
        return true
      } else {
        // Unexpected error, no jwt and/or user
        throw {
          response: {
            error: 'Internal Server Error',
            statusCode: 500,
            data: [{
              id: 'Auth.form.error.unexpected',
              message: 'An unexpected error occured, please contact the System Administrator'
            }],
            message: [{
              id: 'Auth.form.error.unexpected',
              message: 'An unexpected error occured, please contact the System Administrator'
            }]
          }
        }
      }
    } catch (error) {
      throw error.response
    }
  }
}

export const getters = {
  user: (state) => state.user,
  isLoggedIn: (state) => state.isLoggedIn,
  isUser: (state) => state.isUser,
  isAdmin: (state) => state.isAdmin,
  isAuthor: (state) => state.isAuthor,
  isEditor: (state) => state.isEditor,
}
